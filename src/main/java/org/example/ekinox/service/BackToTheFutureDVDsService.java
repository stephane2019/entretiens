package org.example.ekinox.service;

public class BackToTheFutureDVDsService {
    /**
     * Permet de calculer le prix total de film back to the future en fonction du nombre.
     *
     * @param countFilmsBackToTheFuture le nombre de film back to the future.
     * @return le montant total de film back to the future.
     */
    public double calculerPrixBackToTheFuture(int countFilmsBackToTheFuture) {
        double prixTotal = countFilmsBackToTheFuture * 15;
        if (countFilmsBackToTheFuture >= 3) {
            prixTotal *= 0.8; // 20% de réduction
        } else if (countFilmsBackToTheFuture == 2) {
            prixTotal *= 0.9; // 10% de réduction
        }
        return prixTotal;
    }

    /**
     * Permet de calculer le prix des autres films en fonction du nombre.
     *
     * @param countAutresFilms le nombres des autres films.
     * @return le montant des autres film.
     */
    public double calculerPrixAutresFilms(int countAutresFilms) {
        return countAutresFilms * 20; // Autres films à 20€
    }
}

package org.example.ekinox;

import org.example.ekinox.service.BackToTheFutureDVDsService;

import java.util.List;

public class BackToTheFutureDVDs implements IBackToTheFutureDVDs {
    private final BackToTheFutureDVDsService backToTheFutureDVDsService;

    /**
     * Constructeur paramétré.
     *
     * @param backToTheFutureDVDsService le service.
     */
    public BackToTheFutureDVDs(BackToTheFutureDVDsService backToTheFutureDVDsService) {
        this.backToTheFutureDVDsService = backToTheFutureDVDsService;
    }

    /**
     * Permet de calculer le prix des achats en fonction de la listes des articles.
     *
     * @param filmsAchetes la liste des films achétés.
     * @return le prix total des achats effectués.
     */
    @Override
    public double calculerPrixTotal(List<String> filmsAchetes) {
        int countFilmsBackToTheFuture = 0;
        int countAutresFilms = 0;

        // parcours des films achetés pour determiner le nombre de film
        for (String film : filmsAchetes) {
            if (film.startsWith("Back to the Future")) {
                countFilmsBackToTheFuture++;
            } else {
                countAutresFilms++;
            }
        }

        double prixTotal = backToTheFutureDVDsService.calculerPrixBackToTheFuture(countFilmsBackToTheFuture);
        prixTotal += backToTheFutureDVDsService.calculerPrixAutresFilms(countAutresFilms);

        return prixTotal;
    }
}

package org.example.ekinox;

import java.util.List;

public interface IBackToTheFutureDVDs {
    double calculerPrixTotal(List<String> filmsAchetes);
}

import org.example.ekinox.BackToTheFutureDVDs;
import org.example.ekinox.service.BackToTheFutureDVDsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class BackToTheFutureDVDsTest {

    private BackToTheFutureDVDsService backToTheFutureDVDsService;
    private BackToTheFutureDVDs backToTheFutureDVDs;

    @BeforeEach
    public void setUp() {
        backToTheFutureDVDsService = mock(BackToTheFutureDVDsService.class); // mock du service
        backToTheFutureDVDs = new BackToTheFutureDVDs(backToTheFutureDVDsService);
    }

    @Test
    public void shouldCalculerPrixTotal_andThreeFuture() {
        // GIVEN
        List<String> filmsAchetes = Arrays.asList(
                "Back to the Future 1",
                "Back to the Future 2",
                "Back to the Future 3"
        );

        when(backToTheFutureDVDsService.calculerPrixBackToTheFuture(3)).thenReturn(36.0);
        when(backToTheFutureDVDsService.calculerPrixAutresFilms(0)).thenReturn(0.0);

        // WHEN
        double prixTotal = backToTheFutureDVDs.calculerPrixTotal(filmsAchetes);

        // THEN
        assertEquals(36, prixTotal);

        // Vérifiez les appels au service
        verify(backToTheFutureDVDsService).calculerPrixBackToTheFuture(3);
        verify(backToTheFutureDVDsService).calculerPrixAutresFilms(0);
    }

    @Test
    public void shouldCalculerPrixTotal_andTwoFuture() {
        // GIVEN
        List<String> filmsAchetes = Arrays.asList(
                "Back to the Future 1",
                "Back to the Future 3"
        );

        when(backToTheFutureDVDsService.calculerPrixBackToTheFuture(2)).thenReturn(27.0);
        when(backToTheFutureDVDsService.calculerPrixAutresFilms(0)).thenReturn(0.0);

        // WHEN
        double prixTotal = backToTheFutureDVDs.calculerPrixTotal(filmsAchetes);

        // THEN
        assertEquals(27, prixTotal);

        // Vérifiez les appels au service
        verify(backToTheFutureDVDsService).calculerPrixBackToTheFuture(2);
        verify(backToTheFutureDVDsService).calculerPrixAutresFilms(0);
    }

    @Test
    public void shouldCalculerPrixTotal_andOneFuture() {
        // GIVEN
        List<String> filmsAchetes = List.of("Back to the Future 1");

        when(backToTheFutureDVDsService.calculerPrixBackToTheFuture(1)).thenReturn(15.0);
        when(backToTheFutureDVDsService.calculerPrixAutresFilms(0)).thenReturn(0.0);

        // WHEN
        double prixTotal = backToTheFutureDVDs.calculerPrixTotal(filmsAchetes);

        // THEN
        assertEquals(15, prixTotal);

        // Vérifiez les appels au service
        verify(backToTheFutureDVDsService).calculerPrixBackToTheFuture(1);
        verify(backToTheFutureDVDsService).calculerPrixAutresFilms(0);
    }

    @Test
    public void shouldCalculerPrixTotal_andFourFuture_WithDoublon() {
        // GIVEN
        List<String> filmsAchetes = Arrays.asList(
                "Back to the Future 1",
                "Back to the Future 2",
                "Back to the Future 3",
                "Back to the Future 2"
        );

        when(backToTheFutureDVDsService.calculerPrixBackToTheFuture(4)).thenReturn(48.0);
        when(backToTheFutureDVDsService.calculerPrixAutresFilms(0)).thenReturn(0.0);

        // WHEN
        double prixTotal = backToTheFutureDVDs.calculerPrixTotal(filmsAchetes);

        // THEN
        assertEquals(48, prixTotal);

        // Vérifiez les appels au service
        verify(backToTheFutureDVDsService).calculerPrixBackToTheFuture(4);
        verify(backToTheFutureDVDsService).calculerPrixAutresFilms(0);
    }

    @Test
    public void shouldCalculerPrixTotal_andFourFuture_WithAutresFilm() {
        // GIVEN
        List<String> filmsAchetes = Arrays.asList(
                "Back to the Future 1",
                "Back to the Future 2",
                "Back to the Future 3",
                "La chèvre"
        );

        when(backToTheFutureDVDsService.calculerPrixBackToTheFuture(3)).thenReturn(36.0);
        when(backToTheFutureDVDsService.calculerPrixAutresFilms(1)).thenReturn(20.0);

        // WHEN
        double prixTotal = backToTheFutureDVDs.calculerPrixTotal(filmsAchetes);

        // THEN
        assertEquals(56, prixTotal);

        // Vérifiez les appels au service
        verify(backToTheFutureDVDsService).calculerPrixBackToTheFuture(3);
        verify(backToTheFutureDVDsService).calculerPrixAutresFilms(1);
    }
}